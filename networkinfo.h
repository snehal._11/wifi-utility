#ifndef NETWORKINFO_H
#define NETWORKINFO_H

#include <QObject>
#include <qqml.h>

class NetworkInfo : public QObject {
  Q_OBJECT
  QML_ELEMENT
 public:
  explicit NetworkInfo(QObject* parent = nullptr);
  NetworkInfo(const NetworkInfo& rhs);

  Q_PROPERTY(QString ssid READ getssid WRITE setSsid NOTIFY ssidChanged)
  Q_PROPERTY(double signal READ getSignal WRITE setSignal NOTIFY signalChanged)
  Q_PROPERTY(QString authentication READ getAuthentication WRITE setAuthentication NOTIFY authenticationChanged)
  Q_PROPERTY(QString macAddr READ getMacAddr WRITE setMacAddr NOTIFY macAddrChanged)
  Q_PROPERTY(bool networkStatus READ getNetworkStatus WRITE setNetworkStatus NOTIFY networkStatusChanged)
  Q_PROPERTY(bool storedNetWorkStatus READ getStoredNetWorkStatus WRITE setStoredNetWorkStatus NOTIFY storedNetWorkStatusChanged)

  QString getssid() const;
  void setSsid(const QString& newSsid);

  double getSignal() const;
  void setSignal(double newSignal);

  QString getAuthentication() const;
  void setAuthentication(const QString& newAuthentication);

  QString getMacAddr() const;
  void setMacAddr(const QString& newMacAddr);

  bool getNetworkStatus() const;
  void setNetworkStatus(bool newNetworkStatus);

  bool getStoredNetWorkStatus() const;
  void setStoredNetWorkStatus(bool newStoredNetWorkStatus);

 signals:

  void ssidChanged();
  void signalChanged();

  void authenticationChanged();

  void macAddrChanged();

  void networkStatusChanged();

  void storedNetWorkStatusChanged();

 private:
  QString m_ssid;
  double m_signal;
  QString m_authentication;
  QString m_macAddr;
  bool m_networkStatus;
  bool m_storedNetWorkStatus;
};

#endif // NETWORKINFO_H
