import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0


Dialog{
    id : connectionDialog
    width:500
    height:400

    property int nDialogRadius: 20
    property alias networkName: name.text
    property alias networkPassword: passInput.text


    x: (appWindow.width - width) / 2
    y: (appWindow.height - height) / 2
    parent: ApplicationWindow.overlay

    modal: true

    onOpened: {
    }

    onClosed: {
    }
    onAccepted: {
    }
    background: Rectangle {
        anchors.fill: parent

        color: "#ffffff"
        radius: nDialogRadius
        z: -1

        layer.enabled: true
    }
    contentItem:  Rectangle {
        id: rectangle
        width:parent.width
        height: parent.height
        anchors.fill: parent
        color: "#00000000"
        radius: nDialogRadius

        Rectangle {
            id: headerRect
            width:parent.width
            height: parent.height * 0.1
            anchors.top: parent.top
            radius: nDialogRadius

            Text {
                id: titleLabel
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: "grey"
                text: "Connection"
                font.pointSize: 14
                font.capitalization: Font.AllUppercase
                wrapMode: Text.WordWrap
                renderType: Text.NativeRendering;
            }
            Rectangle{
                id:seperator
                width:parent.width
                height: 2
                border.color: "grey"
                anchors.top:titleLabel.bottom
                anchors.topMargin: 20
            }
        }
    }
    Rectangle{
        id:dataRect
        width: parent.width
        height: parent.height * 0.6
        anchors.verticalCenter: parent.verticalCenter
        Rectangle{
            id:nameRect
            width:parent.width * 0.9
            height: parent.height * 0.2
            anchors{
                top:parent.top
                topMargin: parent.height * 0.1
                horizontalCenter: parent.horizontalCenter
            }
            Row{
                id:nameRow
                width:parent.width
                spacing : 25
                Text {
                    id: nameLable
                    text: "Netwok Name : "
                    font.pixelSize: 16
                    width:parent.width * 0.3
                }
                Text {
                    id: name
                    text: ""
                    font.pixelSize: 16
                    width:parent.width * 0.7
                }
            }
        }
        Rectangle{
            id:passwordRect
            width:parent.width * 0.9
            height: parent.height * 0.2
            anchors{
                top:nameRect.bottom
                horizontalCenter: parent.horizontalCenter
            }
            Row{
                id:passwordRow
                width:parent.width
                spacing : 25
                Text {
                    id: passwordLable
                    text: "Enter Password : "
                    font.pixelSize: 16
                    width:parent.width * 0.3
                }
                TextField{
                    id:passInput
                    width:nameRect.width * 0.6
                    height: nameRect.height
                    echoMode: TextInput.PasswordEchoOnEdit
                }
            }
        }
    }
    Rectangle {
        id: footerRect
        width:parent.width
        height: parent.height * 0.2
        anchors.bottom: parent.bottom
        radius: nDialogRadius
        Button{
            id:okButton
            width : parent.width * 0.25
            height: parent.height * 0.4
            text:"OK"
            anchors{
                verticalCenter: parent.verticalCenter
                left:parent.left
                leftMargin: parent.width * 0.1
            }
            onClicked: accept()
        }
        Button{
            id:cancleButton
            width : parent.width * 0.25
            height: parent.height * 0.4
            text: "Cancel"
            anchors{
                verticalCenter: parent.verticalCenter
                right:parent.right
                rightMargin:  parent.width * 0.1
            }
            onClicked: reject()
        }
    }
}
