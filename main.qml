import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import ConnectionToSsh 1.0

Window {
    id:appWindow
    width: Screen.width
    height: Screen.height
    visible: true
    title: qsTr("Wi-Fi Utility")

    property var wifiList
    property string wifiName

    ConnectionToSsh{
        id:connectToSSH
        onStatusChanged: {
            deviceMsg.text = connectToSSH.getStatus()
        }
        onWifiNetworkChanged: {
             wifiList = connectToSSH.getNetworkList()
        }
    }
    NetworkConnectionPopup{
        id:popup
        networkName:wifiName
        onAccepted: {
            console.log("pass : ",popup.networkPassword)
            connectToSSH.connectToNetwork(wifiName,popup.networkPassword)
            popup.networkPassword = ""
        }
    }

    Rectangle{
        id:sshConnectionRect
        width: parent.width * 0.5
        height: parent.height * 0.9
        border.color: "black"
        GroupBox{
            id:sshConnectionBox
            title: "SSH Connection"
            width:parent.width *0.9
            height: parent.height * 0.5
            anchors{
                margins: 20
                top:parent.top
                topMargin: parent.height * 0.05
                horizontalCenter: parent.horizontalCenter
            }
            Rectangle{
                id:hostRect
                width:parent.width
                height: parent.height * 0.12
                Row{
                    id:hostRow
                    spacing : 25
                    Text {
                        id: hostLable
                        text: "Host : "
                        font.pixelSize: 16
                        width:hostRect.width * 0.1
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    TextField{
                        id:hostInput
                        width:hostRect.width * 0.8
                        height: hostRect.height
                        text:"10.10.70.2"
                        readOnly: true
                        enabled: false
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            Rectangle{
                id:userNameRect
                width:parent.width
                height: parent.height * 0.12
                anchors{
                    top:hostRect.bottom
                    topMargin: userNameRect.height * 0.2
                }
                Row{
                    id:userNameRow
                    spacing : 25
                    Text {
                        id: userNameLable
                        text: "User Name : "
                        font.pixelSize: 16
                        width:hostRect.width * 0.1
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    TextField{
                        id:userNameInput
                        width:hostRect.width * 0.8
                        height: hostRect.height
                        text:"root"
                        readOnly: true
                        enabled: false
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            Rectangle{
                id:portRect
                width:parent.width
                height: parent.height * 0.12
                anchors{
                    top:userNameRect.bottom
                    topMargin: userNameRect.height * 0.2
                }
                Row{
                    id:portRow
                    spacing : 25
                    Text {
                        id: portLable
                        text: "Port : "
                        font.pixelSize: 16
                        width:hostRect.width * 0.1
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    TextField{
                        id:portInput
                        width:hostRect.width * 0.8
                        height: hostRect.height
                        text: "22"
                        readOnly: true
                        enabled: false
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            Rectangle{
                id:passwordRect
                width:parent.width
                height: parent.height * 0.12
                anchors{
                    top:portRect.bottom
                    topMargin: userNameRect.height * 0.2
                }
                Row{
                    id:passwordRow
                    spacing : 25
                    Text {
                        id: passwordLable
                        text: "Password : "
                        font.pixelSize: 16
                        width:hostRect.width * 0.1
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    TextField{
                        id:passwordInput
                        width:hostRect.width * 0.8
                        height: hostRect.height
                        text:"mSB1RiDJcy2XG3/5msTgSBTs^H9aCKr"
                        echoMode: TextInput.PasswordEchoOnEdit
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            Rectangle{
                id:buttonRect
                width: parent.width
                height: parent.height * 0.15
                anchors{
                    top:passwordRect.bottom
                    topMargin: userNameRect.height * 0.5
                }
                Row{
                    id:buttonRow
                    spacing: 40
                    leftPadding: 250
                    Button{
                        id:disconnect
                        text: "Disconnect"
                        onClicked: {
                            connectToSSH.disconnectedSSH()
                            console.log("In qml Disconnected")
                        }
                    }
                    Button{
                        id:connect
                        text:"Connect"
                        onClicked: {
                            if(hostInput.text !="" && userNameInput.text != "" && passwordInput.text != ""){
                                connectToSSH.connectSSH(hostInput.text,userNameInput.text,parseInt(portInput.text),passwordInput.text);
                                console.log("In qml Connected")
                            }
                            else{
                                console.log("Please enter the details.")
                            }
                        }
                    }
                }
            }
            Rectangle{
                id:cmdRect
                width:parent.width
                height: parent.height * 0.12
                anchors{
                    top:buttonRect.bottom
                    topMargin: userNameRect.height * 0.5
                }
                Row{
                    id:cmdRow
                    spacing : 25
                    TextField{
                        id:cmdInput
                        width:hostRect.width * 0.8
                        height: hostRect.height
                        placeholderText: "Cmd"
                        text:""
                        anchors{
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    Button{
                        id:sndButton
                        text:"Send"
                        onClicked: {
                            connectToSSH.sendCommand(cmdInput.text)
                            console.log("Command: ",cmdInput.text)
                            cmdInput.text = ""
                        }
                    }
                }
            }
        }
        Rectangle{
            id:msgRect
            width:parent.width *0.9
            height: parent.height * 0.4
            border.color: "grey"
            anchors{
                margins: 20
                top:sshConnectionBox.bottom
                horizontalCenter: parent.horizontalCenter
            }
            ScrollView{
                id:deviceMsgScrollView
                anchors.fill: parent
                ScrollBar.vertical.policy: ScrollBar.AsNeeded
                ScrollBar.horizontal.policy: ScrollBar.AsNeeded
                TextArea{
                    id:deviceMsg
                    width:parent.width *0.9
                    placeholderText: "Device data"
                    selectByMouse :true
                }

            }
            Button{
                id:clearButton
                text:"Clear"
                width:msgRect.width*0.1
                height: msgRect.height * 0.1
                anchors{
                    right:msgRect.right
                    top:msgRect.top
                }
                onClicked:{
                    deviceMsg.clear()
                    connectToSSH.clearStatus()
                }
            }
        }
    }
    Rectangle{
        id:netwokRect
        width: parent.width * 0.5
        height: parent.height * 0.9
        border.color: "black"
        anchors{
            left:sshConnectionRect.right
        }
        Button{
            id:scanNetwork
            text:"Scan Network"
            anchors{
                top:parent.top
                topMargin: parent.height * 0.05
                horizontalCenter: parent.horizontalCenter
            }
            onClicked: {
                connectToSSH.scanNetwork()
            }
        }
        Rectangle{
            id:listViewRect
            width:parent.width * 0.9
            height: parent.height * 0.86
            anchors{
                top:scanNetwork.bottom
                topMargin: 20
                horizontalCenter: parent.horizontalCenter
            }
            ListView{
                id:listView
                width:parent.width
                height: parent.height
                model:wifiList
                delegate: Rectangle{
                    id:delegate
                    width:parent.width
                    height:50
                    Rectangle{
                        id:statusRect
                        width:20
                        height: 20
                        radius: 10
                        color: "green"
                        visible: model.modelData.networkStatus ? true : false
                        anchors{
                            verticalCenter: parent.verticalCenter
                            left:parent.left
                            leftMargin: parent.width * 0.05
                        }
                    }
                    Rectangle{
                        id:storedRect
                        width:20
                        height: 20
                        radius: 10
                        color: "red"
                        visible: model.modelData.storedNetwork ? true : false
                        anchors{
                            verticalCenter: parent.verticalCenter
                            left:parent.left
                            leftMargin: parent.width * 0.1
                        }
                    }
                    Text{
                        id:networkName
                        anchors{
                            verticalCenter: parent.verticalCenter
                            left:parent.left
                            leftMargin: parent.width * 0.16
                        }
                        text:String(model.modelData.networkSSID)
                    }
                    Text{
                        id:networkSignal
                        anchors{
                            verticalCenter: parent.verticalCenter
                            right:parent.right
                            rightMargin: parent.width * 0.05
                        }
                        text:String(model.modelData.networkSignal)
                    }
                    Rectangle{
                        id:separate
                        width:parent.width
                        height: 2
                        border.color: "grey"
                        anchors{
                            top:networkName.bottom
                            topMargin: 15
                        }
                    }
                    MouseArea{
                        id:networkMouseArea
                        anchors.fill: parent
                        onClicked: {
                            wifiName = model.modelData.networkSSID
                            console.log("network : ",model.modelData.networkSSID)
                            popup.open()
                        }
                    }

                }
            }
        }
    }
}
