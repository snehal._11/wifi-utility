#include "connectiontossh.h"
#include "networkinfo.h"
#include <QTimer>

Q_DECLARE_METATYPE(NetworkInfo);

QString ConnectionToSsh::cmd_connectedNetwork = "iw INERFACENAMEPLACEHOLDER link";
QString ConnectionToSsh::cmd_scanNetwork = "iw INERFACENAMEPLACEHOLDER scan";
QString ConnectionToSsh::cmd_connection = "wpa_passphrase NETWORKNAME  NETWORKPASSWORD >> /etc/wpa_supplicant.conf";
QString ConnectionToSsh::cmd_showLink = "ip link show INERFACENAMEPLACEHOLDER";
QString ConnectionToSsh::cmd_setLinkToUp = "ip link set INERFACENAMEPLACEHOLDER up";
QString ConnectionToSsh::cmd_wpaSupplicant = "wpa_supplicant -B -i INERFACENAMEPLACEHOLDER -c /etc/wpa_supplicant.conf";
QString ConnectionToSsh::cmd_udhcpc = "udhcpc -i INERFACENAMEPLACEHOLDER";

ConnectionToSsh::ConnectionToSsh(QObject* parent)
  : QAbstractListModel{parent} {

}

void ConnectionToSsh::connectSSH(const QString& host, const QString& userName, const int& port, const QString& password) {
  int timeout = 5;
  mParams.host = host;
  mParams.userName = userName;
  mParams.port = port;
  mParams.password = password;

  mParams.timeout = timeout;
  mParams.authenticationType = SshConnectionParameters::AuthenticationTypePassword;
  mParams.options = SshIgnoreDefaultProxy;
  mParams.hostKeyCheckingMode = SshHostKeyCheckingNone;

  mConnections.reset();
  mConnections = std::make_shared<SshConnection>(mParams);

  connect(mConnections.get(), &SshConnection::error, this, &ConnectionToSsh::sshConnectionError);
  connect(mConnections.get(), &SshConnection::connected, this, &ConnectionToSsh::sshConnectionConnected);
  connect(mConnections.get(), &SshConnection::disconnected, this, &ConnectionToSsh::sshConnectionDisconnected);
  connect(mConnections.get(), &SshConnection::dataAvailable, this, &ConnectionToSsh::sshConnectionDataAvailable);

  mConnections->connectToHost();
}

void ConnectionToSsh::disconnectedSSH() {
  emit mConnections->disconnected();
  connect(mConnections.get(), &SshConnection::disconnected, this, &ConnectionToSsh::sshConnectionDisconnected);
}

void ConnectionToSsh::sendCommand(const QString& cmd, ReadyReadOutputType readyReadOutput) {
  if (cmd != "") {
    m_recentCmd = cmd;
    mRemoteProcess = mConnections->createRemoteProcess(cmd.toUtf8());
    if (mRemoteProcess) {
      setStatus("Command " + cmd + " was sent.");
    } else {
      setStatus("Error: UnmRemoteProcess SSH connection creates remote process.");
      return;
    }

    connect(mRemoteProcess.data(), &SshRemoteProcess::started, this, &ConnectionToSsh::sshRemoteProcessStarted);
    connect(mRemoteProcess.data(), &SshRemoteProcess::readyReadStandardError, this, &ConnectionToSsh::sshRemoreProcessReadyReadStandardError);
    connect(mRemoteProcess.data(), &SshRemoteProcess::closed, this, &ConnectionToSsh::sshRemoteProcessClosed);
    if (readyReadOutput != nullptr) {
      connect(mRemoteProcess.data(), &SshRemoteProcess::readyReadStandardOutput, this, readyReadOutput);
      mRemoteProcess->start();
    } else {
      connect(mRemoteProcess.data(), &SshRemoteProcess::readyReadStandardOutput, this, &ConnectionToSsh::sshRemoteProcessReadyReadStandardOutput);
      mRemoteProcess->start();
    }
  }

}

void ConnectionToSsh::sshConnectionConnected() {
  qDebug() << "SSH Connected.";
  setStatus("Connected");
}

void ConnectionToSsh::sshConnectionDisconnected() {
  qDebug() << "SSH Disconnected.";
  setStatus("Disconnected");
  disconnect(mConnections.get(), &SshConnection::error, this, &ConnectionToSsh::sshConnectionError);
  disconnect(mConnections.get(), &SshConnection::connected, this, &ConnectionToSsh::sshConnectionConnected);
  disconnect(mConnections.get(), &SshConnection::disconnected, this, &ConnectionToSsh::sshConnectionDisconnected);
  disconnect(mConnections.get(), &SshConnection::dataAvailable, this, &ConnectionToSsh::sshConnectionDataAvailable);
}

void ConnectionToSsh::sshConnectionError(SshError error) {
  Q_UNUSED(error)

  QString errorString;

  errorString = "Error: " + mConnections->errorString();
  setStatus(errorString);
}

void ConnectionToSsh::sshConnectionDataAvailable(const QString& message) {
  setStatus(message);
}

void ConnectionToSsh::sshRemoteProcessStarted() {
  setStatus("Remote Process Started");
}

void ConnectionToSsh::sshRemoteProcessReadyReadStandardOutput() {
  QString outputString;

  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
}

void ConnectionToSsh::sshRemoreProcessReadyReadStandardError() {
  QString errorString;

  errorString = mRemoteProcess->readAllStandardError();
  setStatus(errorString);
}

void ConnectionToSsh::sshRemoteProcessClosed(const int& errorCode) {
  QString exitStatus;

  exitStatus = "ExitStatus : " + QString::number(errorCode);
  setStatus(exitStatus);

}

void ConnectionToSsh::scanInterfaceReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
  outputString.remove("\t");
  QStringList parseDataList = outputString.split("\n");
  QStringList list = parseDataList[1].split(" ");
  setInterface(list[1]);

  searchConnectedNetwork();

}

void ConnectionToSsh::searchConnectedNetworkReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
  m_storedconnectedNetwork = get_connected_network(outputString);
  scanNetworkList();
}

void ConnectionToSsh::scanNetworkListReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
  get_network_details(outputString);
}

void ConnectionToSsh::connectToNewNetworkReadyRead() {
  qDebug() << "in connectToNewNetworkReadyRead";
}

void ConnectionToSsh::checkContentWpaSupplicantReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
  get_stored_network(outputString);
  for (const QString& ssid : m_storedNetworkList) {
    if (m_selectedNetwork == ssid) {
      get_wpa_supplicant();
      break;
    }//TODO:success popup
  }
}

void ConnectionToSsh::get_wpa_supplicantReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
  QStringList dataList = outputString.split("\n");
  if (dataList.contains("Successfully initialized wpa_supplicant")) {
    QTimer::singleShot(5000, this, &ConnectionToSsh::getConnectedNetwork);
  }
}

void ConnectionToSsh::getConnectedNetworkReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
  qDebug() << "getConnectedNetworkReadyRead:" << m_storedconnectedNetwork;
  if (!m_storedconnectedNetwork.isEmpty()) {
    udhcpc();
  }
}

void ConnectionToSsh::udhcpcReadyRead() {
  QString outputString;
  outputString = QString::fromLatin1(mRemoteProcess->readAllStandardOutput());
  setStatus(outputString);
}

int ConnectionToSsh::rowCount(const QModelIndex& parent) const {
  Q_UNUSED(parent);
  return m_wifiNetwork.length();
}

QVariant ConnectionToSsh::data(const QModelIndex& index, int role) const {
  if (index.isValid() && index.row() >= 0 && index.row() < m_wifiNetwork.length()) {
    NetworkInfo* networkInfo = m_wifiNetwork[index.row()];

    switch ((Role) role) {
      case NetworkSSIDRole:
        return networkInfo->getssid();
      case NetworkSignalRole:
        return networkInfo->getSignal();
      case NetworkAuthenticationRole:
        return networkInfo->getAuthentication();
      case  NetworkMacAddrRole:
        return networkInfo->getMacAddr();
      case NetworkStatusRole:
        return networkInfo->getNetworkStatus();
      case NetworkStoredNetworkRole:
        return networkInfo->getStoredNetWorkStatus();
    }
  }
  return {};
}

QHash<int, QByteArray> ConnectionToSsh::roleNames() const {
  QHash<int, QByteArray> result;
  result[NetworkSSIDRole] = "networkSSID";
  result[NetworkSignalRole] = "networkSignal";
  result[NetworkAuthenticationRole] = "networkAuth";
  result[NetworkMacAddrRole] = "networkMacAddr";
  result[NetworkStatusRole] = "networkStatus";
  result[NetworkStoredNetworkRole] = "storedNetwork";

  return result;
}


void ConnectionToSsh::setStatus(const QString& newStatus) {
  if (m_status == newStatus)
    return;
  m_status.append(newStatus);
  m_status.append("\n");
  emit statusChanged();
}

void ConnectionToSsh::scanInterface() {
  QString cmd = cmd_iterface;
  sendCommand(cmd, &ConnectionToSsh::scanInterfaceReadyRead);
}

void ConnectionToSsh::connectToNewNetwork(const QString& name, const QString& password) {
  m_selectedNetwork = name;
  if (name.contains(" ")) {
    qDebug() << "NAME:" << "\"" + name + "\"";
    cmd_connection.replace("NETWORKNAME", "\"" + name + "\"");
    cmd_connection.replace("NETWORKPASSWORD", password);
    qDebug() << "cmd_connection : " << cmd_connection;
  } else {
    cmd_connection.replace("NETWORKNAME", name);
    cmd_connection.replace("NETWORKPASSWORD", password);
    qDebug() << "cmd_connection : " << cmd_connection;
  }
  QString cmd = cmd_connection;
  sendCommand(cmd, &ConnectionToSsh::connectToNewNetworkReadyRead);
  QTimer::singleShot(1000, this, &ConnectionToSsh::checkContentWpaSupplicant);
}

void ConnectionToSsh::checkContentWpaSupplicant() {
  sendCommand(cmd_checkContent_wpa_supplicant, &ConnectionToSsh::checkContentWpaSupplicantReadyRead);
}

void ConnectionToSsh::get_wpa_supplicant() {
  if (m_interface != nullptr) {
    cmd_wpaSupplicant.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_wpaSupplicant;
    sendCommand(cmd, &ConnectionToSsh::get_wpa_supplicantReadyRead);
  }
}

void ConnectionToSsh::getConnectedNetwork() {
  if (m_interface != nullptr) {
    cmd_connectedNetwork.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_connectedNetwork;
    sendCommand(cmd, &ConnectionToSsh::getConnectedNetworkReadyRead);
  }
}

void ConnectionToSsh::udhcpc() {
  if (m_interface != nullptr) {
    cmd_udhcpc.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_udhcpc;
    sendCommand(cmd, &ConnectionToSsh::udhcpcReadyRead);
  }
}

void ConnectionToSsh::showLink() {
  if (m_interface != nullptr) {
    cmd_showLink.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_showLink;
    sendCommand(cmd);
  }
}

void ConnectionToSsh::setLinkToUp() {
  if (m_interface != nullptr) {
    cmd_setLinkToUp.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_setLinkToUp;
    sendCommand(cmd);
  }
}

void ConnectionToSsh::searchConnectedNetwork() {
  if (m_interface != nullptr) {
    cmd_connectedNetwork.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_connectedNetwork;
    sendCommand(cmd, &ConnectionToSsh::searchConnectedNetworkReadyRead);
  }
}

void ConnectionToSsh::scanNetworkList() {
  m_wifiNetwork.clear();
  if (m_interface != nullptr) {
    cmd_scanNetwork.replace("INERFACENAMEPLACEHOLDER", m_interface);
    QString cmd = cmd_scanNetwork;
    sendCommand(cmd, &ConnectionToSsh::scanNetworkListReadyRead);
  }
}

QString ConnectionToSsh::getStatus() {
  return m_status;
}

void ConnectionToSsh::scanNetwork() {
  connectionFlow = false;
  scanInterface();
}
void ConnectionToSsh::connectToNetwork(const QString& name, const QString& password) {
  connectionFlow = true;
  connectToNewNetwork(name, password);
}

QVariantList ConnectionToSsh::getNetworkList() {
  QVariantList networkList;
  for (NetworkInfo* network  : m_wifiNetwork) {
    QVariantMap networkMap;
    networkMap["networkSSID"] = network->getssid();
    networkMap["networkSignal"] = network->getSignal();
    networkMap["networkAuth"] = network->getAuthentication();
    networkMap["networkMacAddr"] = network->getMacAddr();
    networkMap["networkStatus"] = network->getNetworkStatus();
    networkList.append(networkMap);
  }
  return networkList;
}

void ConnectionToSsh::clearStatus() {
  m_status.clear();
  emit statusChanged();

}

QString ConnectionToSsh::getInterface() const {
  return m_interface;
}

void ConnectionToSsh::setInterface(const QString& newInterface) {
  if (m_interface == newInterface)
    return;
  m_interface = newInterface;
  emit interfaceChanged();
}


QString ConnectionToSsh::   get_mac_address(const QStringList& individual_data, int& i) {
  static const int colon_count = 5;
  static const QString& mac_address_identifier = "BSS ";
  QString mac_address = "";
  while (i < individual_data.length()) {
    QString data = individual_data[i];

    if (data.indexOf(mac_address_identifier) != -1 && data.count(":") == colon_count) {
      int from_index = data.indexOf(mac_address_identifier) + mac_address_identifier.length();
      int to_index = data.indexOf("(");
      if (to_index == -1) {
        to_index = data.indexOf(" ");
        if (to_index == -1) {
          to_index = data.length();
        } else {
          to_index = data.length();
        }
      }

      int characters = data.length() - (data.indexOf(mac_address_identifier) + mac_address_identifier.length()) - (data.length() - to_index);
      mac_address = data.mid(from_index, characters);
      ++i;
      break;
    }
    ++i;
  }
  return mac_address;
}

QString ConnectionToSsh::get_ssid(const QStringList& individual_data, int& i) {
  static const QString& ssid_identifier = "SSID: ";
  QString ssid = "";
  while (i < individual_data.length()) {
    QString data = individual_data[i];
    if (data.indexOf(ssid_identifier) != -1) {
      ssid = data.right(data.length() - ssid_identifier.length() - 1);
      ++i;
      break;
    }
    ++i;
  }
  return ssid;
}

QString ConnectionToSsh::get_authentication_suit(const QStringList& individual_data, int& i) {
  static const QString& authentication_suit_identifier = "Authentication suites: ";
  QString authentication_suit = "";
  while (i < individual_data.length()) {
    QString data = individual_data[i];

    if (data.indexOf(authentication_suit_identifier) != -1) {
      authentication_suit = data.right(data.length() - (authentication_suit_identifier.length() + data.indexOf(authentication_suit_identifier)));
      ++i;
      break;
    }
    ++i;
  }
  return authentication_suit;
}

double ConnectionToSsh::get_signal(const QStringList& individual_data, int& i) {
  static const int word_count = 3;
  static const QString& signal_identifier = "signal: ";
  static const QString& Dbm_identifier = " dBm";
  double signal = -1;
  while (i < individual_data.length()) {
    QString data = individual_data[i];

    if (data.indexOf(signal_identifier) != -1 && data.indexOf(Dbm_identifier) != -1) {
      QStringList signal_strings = data.split(" ");
      if (signal_strings.count() == word_count) {
        QString signal_text = signal_strings[1];
        signal = signal_text.toDouble();
        ++i;
      }
      break;
    }
    ++i;
  }
  return signal;
}

bool ConnectionToSsh::get_network_status(const QString& ssid) {
  bool status;
  if (m_storedconnectedNetwork == ssid) {
    status =  true;
  } else {
    status = false;
  }
  return status;
}

bool ConnectionToSsh::get_stored_network_status(const QString& ssid) {
  bool status;
  for (const QString& element : m_storedNetworkList) {
    if (element == ssid) {
      status = true;
    } else {
      status = false;
    }
  }
  return status;
}

QList<NetworkInfo*> ConnectionToSsh::get_network_details(const QString& wifi_network_info) {
  QStringList individual_data = wifi_network_info.split("\n");
  int i = 0;
  while (i < individual_data.length()) {
    QString mac_address = get_mac_address(individual_data, i);
    if (mac_address.isEmpty() != true) {
      double signal = get_signal(individual_data, i);
      if (signal != -1) {
        QString ssid = get_ssid(individual_data, i);
        if (ssid.isEmpty() != true) {
          QString authentication_suit = get_authentication_suit(individual_data, i);
          bool status = get_network_status(ssid);
          bool storedNetworkStatus = get_stored_network_status(ssid);
          if (authentication_suit.isEmpty() != true) {
            NetworkInfo* network_detail = new NetworkInfo();
            network_detail->setMacAddr(mac_address);
            network_detail->setSignal(signal);
            network_detail->setSsid(ssid);
            network_detail->setAuthentication(authentication_suit);
            network_detail->setNetworkStatus(status);
            network_detail->setStoredNetWorkStatus(storedNetworkStatus);
            m_wifiNetwork.push_back(network_detail);
          }
        }
      }
    }
    ++i;
  }
  emit wifiNetworkChanged();
  return m_wifiNetwork;
}

QString ConnectionToSsh::get_connected_network(const QString& network_info) {
  QStringList individual_data = network_info.split("\n");
  QString ssid = nullptr ;
  ssid = individual_data[1].remove("\tSSID: ");
  return ssid;
}

void ConnectionToSsh::get_stored_network(const QString& network_info) {
  QRegularExpression ssidRegex("ssid=\"([^\"]+)\"");

  QStringList ssidList;
  QRegularExpressionMatchIterator matchIterator = ssidRegex.globalMatch(network_info);

  while (matchIterator.hasNext()) {
    QRegularExpressionMatch match = matchIterator.next();
    if (match.hasMatch()) {
      ssidList.append(match.captured(1));
    }
  }
  m_storedNetworkList = ssidList;
}

