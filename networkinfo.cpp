#include "networkinfo.h"

NetworkInfo::NetworkInfo(QObject* parent)
  : QObject{parent} {

}

NetworkInfo::NetworkInfo(const NetworkInfo& rhs) {
  m_ssid = rhs.m_ssid;
  m_signal = rhs.m_signal;
  m_authentication = rhs.m_authentication;
  m_macAddr = rhs.m_macAddr;
  m_networkStatus = rhs.m_networkStatus;
  m_storedNetWorkStatus = rhs.m_storedNetWorkStatus;
}

QString NetworkInfo::getssid() const {
  return m_ssid;
}

void NetworkInfo::setSsid(const QString& newSsid) {
  if (m_ssid == newSsid)
    return;
  m_ssid = newSsid;
  emit ssidChanged();
}

double NetworkInfo::getSignal() const {
  return m_signal;
}

void NetworkInfo::setSignal(double newSignal) {
  if (qFuzzyCompare(m_signal, newSignal))
    return;
  m_signal = newSignal;
  emit signalChanged();
}

QString NetworkInfo::getAuthentication() const {
  return m_authentication;
}

void NetworkInfo::setAuthentication(const QString& newAuthentication) {
  if (m_authentication == newAuthentication)
    return;
  m_authentication = newAuthentication;
  emit authenticationChanged();
}

QString NetworkInfo::getMacAddr() const {
  return m_macAddr;
}

void NetworkInfo::setMacAddr(const QString& newMacAddr) {
  if (m_macAddr == newMacAddr)
    return;
  m_macAddr = newMacAddr;
  emit macAddrChanged();
}

bool NetworkInfo::getNetworkStatus() const {
  return m_networkStatus;
}

void NetworkInfo::setNetworkStatus(bool newNetworkStatus) {
  if (m_networkStatus == newNetworkStatus)
    return;
  m_networkStatus = newNetworkStatus;
  emit networkStatusChanged();
}

bool NetworkInfo::getStoredNetWorkStatus() const {
  return m_storedNetWorkStatus;
}

void NetworkInfo::setStoredNetWorkStatus(bool newStoredNetWorkStatus) {
  if (m_storedNetWorkStatus == newStoredNetWorkStatus)
    return;
  m_storedNetWorkStatus = newStoredNetWorkStatus;
  emit storedNetWorkStatusChanged();
}
