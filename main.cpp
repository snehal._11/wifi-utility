#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "connectiontossh.h"
#include "networkinfo.h"

int main(int argc, char* argv[]) {
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
  QGuiApplication app(argc, argv);

  qmlRegisterType<ConnectionToSsh>("ConnectionToSsh", 1, 0, "ConnectionToSsh");


  qmlRegisterType<NetworkInfo>("NetworkInfo", 1, 0, "NetworkInfo");

  QQmlApplicationEngine engine;
  QQmlContext* context = engine.rootContext();

  ConnectionToSsh model(&app);

  context->setContextProperty("networkModel", QVariant::fromValue(&model));

  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                   &app, [url](QObject * obj, const QUrl & objUrl) {
                       if (!obj && url == objUrl)
                       QCoreApplication::exit(-1);
                   }, Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
