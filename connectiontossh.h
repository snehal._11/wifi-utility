#ifndef CONNECTIONTOSSH_H
#define CONNECTIONTOSSH_H

#include <QObject>
#include <ssh/sshconnection.h>
#include <ssh/sshremoteprocess.h>
#include <QAbstractListModel>
#include <QRegularExpression>
#include "networkinfo.h"


class NetworkInfo;

using namespace QSsh;

class ConnectionToSsh : public QAbstractListModel {
  Q_OBJECT

 public:

  typedef void (ConnectionToSsh::*ReadyReadOutputType)();
  explicit ConnectionToSsh(QObject* parent = nullptr);

  Q_PROPERTY(QString status READ getStatus WRITE setStatus NOTIFY statusChanged)
  Q_PROPERTY(QString interface READ getInterface WRITE setInterface NOTIFY interfaceChanged)

  Q_INVOKABLE void connectSSH(const QString& host, const QString& userName, const int& port, const QString& password);
  Q_INVOKABLE void disconnectedSSH();
  Q_INVOKABLE void sendCommand(const QString& cmd, ReadyReadOutputType readyReadOutput = nullptr);
  Q_INVOKABLE void clearStatus();
  Q_INVOKABLE QString getStatus();
  Q_INVOKABLE void scanNetwork();
  Q_INVOKABLE QVariantList getNetworkList();
  Q_INVOKABLE void connectToNetwork(const QString& name, const QString& password);

  //Functions for send command
  void searchConnectedNetwork();
  void scanNetworkList();
  void setStatus(const QString& newStatus);
  void scanInterface();
  void connectToNewNetwork(const QString& name, const QString& password);
  void checkContentWpaSupplicant();
  void get_wpa_supplicant();
  void getConnectedNetwork();
  void udhcpc();
  void setLinkToUp();
  void showLink();

  void getRecentCommand()const;
  QString getInterface() const;
  void setInterface(const QString& newInterface);


  QString get_mac_address(const QStringList& individual_data, int& i);
  QString get_ssid(const QStringList& individual_data, int& i);
  QString get_authentication_suit(const QStringList& individual_data, int& i);
  double get_signal(const QStringList& individual_data, int& i);
  bool get_network_status(const QString& ssid);
  bool get_stored_network_status(const QString& ssid);

  QList<NetworkInfo*> get_network_details(const QString& wifi_network_info);
  QString get_connected_network(const QString& network_info);
  void get_stored_network(const QString& network_info);

 signals:

  void statusChanged();
  void interfaceChanged();

  void wifiNetworkChanged();

 public slots:
  void sshConnectionConnected();
  void sshConnectionDisconnected();
  void sshConnectionError(QSsh::SshError error);
  void sshConnectionDataAvailable(const QString& message);
  void sshRemoteProcessStarted();
  void sshRemoteProcessReadyReadStandardOutput();
  void sshRemoreProcessReadyReadStandardError();
  void sshRemoteProcessClosed(const int& errorCode);
  void scanInterfaceReadyRead();
  void searchConnectedNetworkReadyRead();
  void scanNetworkListReadyRead();
  void connectToNewNetworkReadyRead();
  void checkContentWpaSupplicantReadyRead();
  void get_wpa_supplicantReadyRead();
  void getConnectedNetworkReadyRead();
  void udhcpcReadyRead();

 private:
  SshConnectionParameters mParams;
  std::shared_ptr<SshConnection> mConnections;
  QSharedPointer<SshRemoteProcess> mRemoteProcess;

  QString m_status = nullptr;
  QString m_recentCmd = nullptr;
  QString m_interface = nullptr;
  QString m_storedconnectedNetwork;
  QString m_selectedNetwork;
  QList<NetworkInfo*> m_wifiNetwork;
  QStringList m_storedNetworkList;
  bool connectionFlow = false;

//Commands

  const QString cmd_iterface = "iw dev";
  const QString cmd_checkContent_wpa_supplicant = "cat /etc/wpa_supplicant.conf";

  static QString cmd_connectedNetwork;
  static QString cmd_scanNetwork;
  static QString cmd_connection ;
  static QString cmd_showLink;
  static QString cmd_setLinkToUp;
  static QString cmd_wpaSupplicant;
  static QString cmd_udhcpc;

  // QAbstractItemModel interface
 public:
  enum Role {
    NetworkSSIDRole = Qt::UserRole + 1,
    NetworkSignalRole,
    NetworkAuthenticationRole,
    NetworkMacAddrRole,
    NetworkStatusRole,
    NetworkStoredNetworkRole
  };
  int rowCount(const QModelIndex& parent) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QHash<int, QByteArray> roleNames() const override;
};

#endif // CONNECTIONTOSSH_H
